package wewei;

public class Repository {
	//I don't have a partner so i'm doing this alone ma'am
 //Name: Rei Krishna Vergara
	
	//recursive
	public static int gcd(int a, int e) {
		//it will iterate until e is equals to 0. 	
		if (e== 0) return a;
		else return gcd(e, a%e);
	}
	//non-recursive
	public static int gcd2(int a, int e) {
		//it will iterate until the e is still unequal or not equal to 0
		while(e != 0) {
			int temp = e;
			//then e will became 0 because of the utilization of modulo(%)
			e = a % e;
			a = temp;
		}
		return a;
	}
	public static void main(String[] args) {
		int a = 102;
		int e = 68;
		int b = gcd(a,e); //The value of b
		int bd = gcd(a, e); //The value bd
		System.out.println("gcd(" + a + ", " + e + ") = " + b);
		System.out.println("gcd(" + a + ", " + e + ") = " + bd);

	}

}
